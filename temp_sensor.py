import os

class TempSensor:
    def __init__(self, temp_sensor):
        self.data = []
        self.temp_sensor = temp_sensor
        
    def read(self, units):
        self.get_data()
        state = self.get_state()
        if not state:
            return (state, 0)

        raw = self.get_raw_temp()
        temp = None

        c = float(raw)/1000
        if units == 'c':
            temp = c
        elif units == 'f':
            temp = c * 9.0/5.0 + 32
        else:
            temp = None

        return (state, temp)

    def get_data(self):
        f = open(self.temp_sensor, 'r')
        self.data = f.readlines()
        f.close()

    def get_state(self):
        try:
            self.data[0].index("YES")
            return True
        except ValueError:
            return False

    def get_raw_temp(self):
        return int(self.data[1][29:])
