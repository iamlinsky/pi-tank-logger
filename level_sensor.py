import VL53L0X
import time

import os
import sys
from contextlib import contextmanager

@contextmanager
def stdout_redirected(to=os.devnull):
    '''
    https://stackoverflow.com/questions/5081657/how-do-i-prevent-a-c-shared-library-to-print-on-stdout-in-python
    
    import os

    with stdout_redirected(to=filename):
        print("from Python")
        os.system("echo non-Python applications are also supported")
    '''
    fd = sys.stdout.fileno()

    ##### assert that Python and C stdio write using the same file descriptor
    ####assert libc.fileno(ctypes.c_void_p.in_dll(libc, "stdout")) == fd == 1

    def _redirect_stdout(to):
        sys.stdout.close() # + implicit flush()
        os.dup2(to.fileno(), fd) # fd writes to 'to' file
        sys.stdout = os.fdopen(fd, 'w') # Python writes to fd

    with os.fdopen(os.dup(fd), 'w') as old_stdout:
        with open(to, 'w') as file:
            _redirect_stdout(to=file)
        try:
            yield # allow code to be run with the redirected stdout
        finally:
            _redirect_stdout(to=old_stdout) # restore stdout.
                                            # buffering and flags such as
                                            # CLOEXEC may be different

class LevelSensor:

    def __init__(self, tank_size, zero_level):
        self.tank_size = tank_size
        self.zero_level = zero_level
        with stdout_redirected():
            self.lidar = VL53L0X.VL53L0X(i2c_bus=1,i2c_address=0x29)

    def read_avg(self, sample_count):
        """
        Take average of raw lidar readings
        Returns -1 if any samples fail
        """
        with stdout_redirected():
            self.lidar.open()
            self.lidar.start_ranging(VL53L0X.Vl53l0xAccuracyMode.BETTER)
            sample_time = self.lidar.get_timing()/1000000.0

            sum = 0.0
            for _ in range(sample_count):
                distance = self.lidar.get_distance()
                if distance > 0:
                    sum += distance
                else:
                    self.lidar.stop_ranging()
                    self.lidar.close()
                    return -1
                time.sleep(sample_time)
            
            self.lidar.stop_ranging()
            self.lidar.close()

        return sum / sample_count

    def get_water_level(self, avg):
        """
        Returns adjusted water level in millimeters from lidar readings
        """
        return self.zero_level - avg

    def get_water_volume(self, level):
        """
        Returns water volume in liters from water level
        """
        cubic_mm =  self.tank_size[0] * self.tank_size[1] * level
        return cubic_mm * 1e-6





