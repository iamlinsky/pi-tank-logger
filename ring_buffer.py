import pickle
import os.path
import sys


class Buffer:
    def __init__(self):
        self.size = -1
        self.pointer = -1
        self.data = []

    def __str__(self):
        return "Size: {} \nPointer: {} \nData: {}".format(self.size, self.pointer, self.data)

class RingBuffer:
    def __init__(self, file, size=-1):
        self.ring = None
        self.buffer_file = file
        self.synced = False

        if not os.path.exists(self.buffer_file):
            if size == -1:
                sys.exit(-1)
            f = open(self.buffer_file, "xb")
            self.ring = Buffer()
            self.ring.size = int(size)
            self.ring.data = [None]*int(size)
            self.ring.pointer = 0
            pickle.dump(self.ring, f)
            f.close()
        else:
            f = open(self.buffer_file, "rb")
            self.ring = pickle.load(f)
            f.close()

        self.synced = True

    def push(self, data):
        """Add entry to ring buffer
        """
        self.ring.data[self.ring.pointer] = data
        self.ring.pointer += 1
        if self.ring.pointer == self.ring.size:
            self.ring.pointer = 0
        self.synced = False

    def commit(self):
        """Write pending changes to ring buffer file
        """
        f = open(self.buffer_file, "wb")
        pickle.dump(self.ring, f)
        f.close()
        self.synced = True

    def get_size(self):
        return self.ring.size

    def read(self, index):
        """Returns entry at specified index.
        Index 0 returns oldest entry
        Index of [ring size] returns newest entry
        """
        f = open(self.buffer_file, "rb")
        ring = pickle.load(f)
        f.close()
        return ring.data[int(ring.pointer + index)%ring.size]

    def read_all(self):
        """Returns entire ring buffer as a list ordered from oldest to newest
        """
        f = open(self.buffer_file, "rb")
        ring = pickle.load(f)
        f.close()
        return ring.data[ring.pointer:] + ring.data[:ring.pointer]

if __name__ == "__main__":
    ring = RingBuffer("test_buffer", 20)
    for i in range(22):
        ring.push(i)
    ring.commit()

    test = RingBuffer("test_buffer", 10)
    
    data = test.read_all()
    print(data)

    print(test.read(0))
    print(test.read(2))
    print(test.read(12))
    print(test.read(test.get_size()-1))