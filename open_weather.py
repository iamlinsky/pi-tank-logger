import requests

class WeatherData:
    def __init__(self):
        self.temp = 0.0
        self.pressure = 0.0
        self.humidity = 0
        self.conditions = 0
        self.sunrise = 0
        self.sunset = 0                           

class OpenWeatherApi:
    def __init__(self, api_key, zip_code):
        self.api_key = api_key
        self._zip_code = zip_code
        self._units = "&units=metric"
        self.weather_request =  "https://api.openweathermap.org/data/2.5/weather?zip={},us{}&appid={}"
        pass

    @property
    def zip_code(self):
        return self._zip_code

    @zip_code.setter
    def zip_code(self, zip_code):
        self._zip_code = zip_code

    @property
    def units(self):
        if self._units == "&units=metric":
            return 'c'
        elif self._units == "&units=imperial":
            return 'f'
        else:
            return 'k'

    @units.setter
    def units(self, char):
        if char == 'c':
            self._units = "&units=metric"
        elif char == 'f':
            self._units = "&units=imperial"
        else:
            self._units = ""

    def get_data(self):
        responce = requests.get(self.weather_request.format(self.zip_code, self._units, self.api_key))
        data = responce.json()

        weather_data = WeatherData()
        weather_data.temp = data["main"]["temp"]
        weather_data.pressure = data["main"]["pressure"]
        weather_data.humidity = data["main"]["humidity"]
        weather_data.sunrise = data["sys"]["sunrise"]
        weather_data.sunset = data["sys"]["sunset"]
        weather_data.conditions = data["weather"][0]["icon"][:2]

        return weather_data


    def get_temp(self):
        responce = requests.get(self.weather_request.format(self.zip_code, self._units, self.api_key))
        data = responce.json()
        return data["main"]["temp"]
