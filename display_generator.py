import sys
import time
from datetime import datetime
from PIL import Image, ImageFont, ImageDraw
import qrcode


class ImageGenerator:
    def __init__(self):
        self.epd_width = 104
        self.epd_height = 212
        self.epd_size = (self.epd_width, self.epd_height)

        self.img_red = Image.new('L', self.epd_size, (255))
        self.img_black = Image.new('L', self.epd_size, (255))
        self.frame_red = ImageDraw.Draw(self.img_red)
        self.frame_black = ImageDraw.Draw(self.img_black)

        self.graph_height = 40
        self.graph_width = 96
        self.graph_origin = (4,100)

        self.temp_max = 70.0
        self.temp_min = 60.0

        self.font_path = "/usr/share/fonts/truetype/freefont/FreeMono.ttf"

    def draw_temp(self, temp):
        font = ImageFont.truetype(self.font_path, 40)
        frame = self.frame_red if temp > self.temp_max or temp < self.temp_min else self.frame_black

        temp_string = "{:.1f}".format(temp)
        
        temp_size = frame.textsize(temp_string, font=font, stroke_width=0, spacing=0)
        text_x = (self.epd_width/2) - (temp_size[0]/2)
        
        frame.text((text_x, -3), temp_string, font=font, spacing=0, fill=0)

    def draw_time(self):
        now = datetime.now()
        time_string = now.strftime("%H:%M")
        font = ImageFont.truetype(self.font_path, 14)
        time_size = self.frame_black.textsize(time_string, font=font, stroke_width=0, spacing=0)
        
        text_x = (self.epd_width - time_size[0]) - 4
        text_y = 38
        self.frame_black.text((text_x, text_y), time_string, font=font, spacing=0, fill=(0))
        self.frame_black.text((4, text_y), "Time:", font=font, spacing=0, fill=(0))

    def draw_seperator(self, padding=4):

        y = self.epd_height - self.epd_width    
    
        points = []
        points.append(padding)
        points.append(y)
        points.append(self.epd_width - padding)
        points.append(y)
        self.frame_black.line(points, 0, 2)

    def scale_temp(self, range, temp):
        span = range[1] - range[0]
        scale = self.graph_height / span      
        point = scale * (temp-range[0])
        return point


        
    def draw_graph(self, temp_list):

        range_min = self.temp_min - 2 if min(temp_list) >= self.temp_min else min(temp_list) - 5
        range_max = self.temp_max + 2 if max(temp_list) <= self.temp_max else max(temp_list) + 5
        range = [range_min, range_max]

        frame_point = []
        frame_point.append(self.graph_origin[0])
        frame_point.append(self.graph_origin[1] - self.graph_height)
        frame_point.append(self.graph_origin[0] + self.graph_width)
        frame_point.append(self.graph_origin[1])

        self.frame_black.rectangle(frame_point, fill=None, outline=(0), width=1)

        x1 = self.graph_origin[0] + 1
        x2 = self.graph_origin[0] + self.graph_width - 1

        y = self.graph_origin[1] - self.scale_temp(range, self.temp_max) + 1
        self.frame_red.line([x1, y, x2, y], 0, 0)
        y = self.graph_origin[1] - self.scale_temp(range, self.temp_min) - 1
        self.frame_red.line([x1, y, x2, y], 0, 0)


        if len(temp_list) == 0:
            pass
        elif len(temp_list) == 1:
            points = []
            temp = self.scale_temp(range, temp_list[0])
            points.append(self.graph_origin[0])
            points.append(self.graph_origin[1] - temp)
            points.append(self.graph_origin[0] + self.graph_width)
            points.append(self.graph_origin[1] - temp) 
            self.frame_black.line(points, 0, 1)
        else:
            w = int(self.graph_width / (len(temp_list)-1))
            point_list = []
            for i, t in enumerate(temp_list):
                x = int(self.graph_origin[0] + (i*w))
                y = int(self.graph_origin[1] - self.scale_temp(range, t))
                point_list.append(x)
                point_list.append(y)
            self.frame_black.line(point_list, 0, 1)        


    def draw_qr(self, data):

        qr = qrcode.QRCode(
            version=None,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            box_size=2,
            border=0,
        )
        qr.add_data(data)
        qr.make(fit=True)

        qr_img = qr.make_image(fill_color="black", back_color="white")
        qr_img = qr_img.resize((96, 96))

        self.img_black.paste(qr_img, (4, (self.epd_height - self.epd_width)+4))

    def get_img_data(self):
        return self.img_black, self.img_red

    def save_image(self, path):
        mask = Image.new('L', self.epd_size, 0)
        blank = Image.new('L', self.epd_size, 255)
        black = Image.merge('RGB', [self.img_black]*3)
        red = Image.merge('RGB', [blank, self.img_red, self.img_red])
        img = Image.composite(black, red, self.img_red)
        img.save(path, "PNG")

if __name__ == "__main__":

    # temp_list = [75.43, 73.2, 70.0, 65.0, 68.0, 69, 63, 58, 57, 58, 60, 63, 64, 64, 66, 66, 66]
    temp_list = [73.625, 73.5116, 73.5116, 73.4, 73.4, 73.5116, 73.73660000000001, 73.73660000000001, 73.73660000000001, 73.73660000000001, 73.625, 73.625, 73.625]

    gen = ImageGenerator()
    gen.draw_temp(temp_list[-1])
    gen.draw_time()
    gen.draw_seperator(4)
    gen.draw_graph(temp_list)
    gen.draw_qr("Some data")
    
    gen.save_image("img.png")

