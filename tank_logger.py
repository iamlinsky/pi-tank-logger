import os
import sys
import json
import time

import psycopg2
import epd2in13b

from temp_sensor import TempSensor
from level_sensor import LevelSensor
from open_weather import OpenWeatherApi
from ring_buffer import RingBuffer
from display_generator import ImageGenerator

class Payload:
    def __init__(self):
        self.timestamp = time.time()
        self.water_temp = 0
        self.water_level = 0
        self.water_volume = 0
        self.air_temp = 0
        self.local_temp = 0
        self.pressure = 0
        self.humidity = 0
        self.conditions = 0

    def get_dict(self):
        return {
        "water_temp" : self.water_temp,
        "water_level" : self.water_level,
        "water_volume" : self.water_volume,
        "air_temp" : self.air_temp,
        "local_temp" : self.local_temp,
        "pressure" : self.pressure,
        "humidity" : self.humidity,
        "conditions" : self.conditions
        }

    def __str__(self):
        return """
        Time:           {}
        Water temp:     {}
        Water level:    {}
        Water volume:   {}
        Air temp:       {}
        Local temp:     {}
        Pressure:       {}
        Humidity:       {}
        Weather:        {}""".format(self.timestamp, self.water_temp, self.water_level, self.water_volume, self.air_temp, self.local_temp, self.pressure, self.humidity, self.conditions)

if __name__ == "__main__":
    # load settings from file
    settings = {}
    with open('settings.json') as settings_file:
        settings = json.load(settings_file)   

    # initialize local storage - 24 hours at 5 minute increments 
    buffer_length = (60/5)*24
    data_buffer = RingBuffer("data_buffer.dat", buffer_length)
    new_data = Payload()

    # initialize sensors
    tank_sensor = TempSensor(settings["tank_temp_sensor_path"])
    air_sensor = TempSensor(settings["air_temp_sensor_path"])
    level_sensor = LevelSensor([200,300], 300)

    # initialize display

    epd = epd2in13b.EPD()
    epd.set_rotate(0)
    epd.init()
    img_gen = ImageGenerator()

    # initialize web interfaces
    weather = OpenWeatherApi(settings["weather_api"]["key"], settings["weather_api"]["zip"])

    rds = psycopg2.connect(
        database=settings["postgres"]["database"], 
        user=settings["postgres"]["user"], 
        password=settings["postgres"]["password"], 
        host=settings["postgres"]["host"], 
        port=settings["postgres"]["port"],
        sslmode="verify-full",
        sslrootcert="/home/pi/.postgresql/rds-ca-2019-root.pem"
        )

    pg_query = "INSERT INTO temp \
        (timestamp, water_temp, water_level, water_volume, air_temp, local_temp, pressure, humidity, conditions) \
        VALUES \
        (now(), %(water_temp)s, %(water_level)s, %(water_volume)s, %(air_temp)s, %(local_temp)s, %(pressure)s, %(humidity)s, %(conditions)s)"

    # get temp sensor values
    tank_sum = 0
    air_sum = 0
    temp_sample_count = 5

    for _ in range(temp_sample_count):
        (tank_state, tank_temp) = tank_sensor.read('c')
        (air_state, air_temp) = air_sensor.read('c')
        if not tank_state or  not air_state:
            sys.exit()
        tank_sum += tank_temp
        air_sum += air_temp
        time.sleep(1)
    
    new_data.tank_temp = tank_sum/temp_sample_count
    new_data.air_temp = air_sum/temp_sample_count

    # get water level
    new_data.water_level = level_sensor.get_water_level(level_sensor.read_avg(10))
    new_data.water_volume = level_sensor.get_water_volume(new_data.water_level)

    # get local weather
    weather_data = weather.get_data()
    new_data.local_temp = weather_data.temp
    new_data.pressure = weather_data.pressure
    new_data.humidity = weather_data.humidity
    new_data.conditions = weather_data.conditions

    # get stored data
    stored_data = data_buffer.read_all()
    stored = stored_data[data_buffer.get_size()-1]
    if stored is not None:
        assert isinstance(stored, Payload)
        delta_t = new_data.tank_temp - stored.water_temp
    
    new_data.timestamp = time.time()
    new_data.water_temp = tank_temp

    # print(new_data)

    data_buffer.push(new_data)
    data_buffer.commit()

    # Send to database
    cursor = rds.cursor()
    cursor.execute(pg_query, new_data.get_dict())
    rds.commit()
    rds.close()

    # Update display
    temp_f = new_data.water_temp * 9.0/5.0 + 32

    index_n = data_buffer.get_size()-1
    graph_range = stored_data[index_n - 12 : index_n]
    if None not in graph_range:
        graph_data = [(d.water_temp * 9.0/5.0 + 32) for d in graph_range] + [temp_f]
    else:
        graph_data = [temp_f]

    img_gen.draw_temp(temp_f)
    img_gen.draw_time()
    img_gen.draw_graph(graph_data)
    img_gen.draw_seperator()
    img_gen.draw_qr(settings["qr_string"])

    img_black, img_red = img_gen.get_img_data()
    frame_black = epd.get_frame_buffer(img_black)
    frame_red = epd.get_frame_buffer(img_red)

    epd.display_frame(frame_black, frame_red)
    